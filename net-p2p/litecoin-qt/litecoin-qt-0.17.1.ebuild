# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DB_VER="4.8"

LANGS="en"

inherit autotools db-use eutils fdo-mime gnome2-utils

MyPV="${PV/_/-}"
MyPN="litecoin"
MyP="${MyPN}-${MyPV}"

DESCRIPTION="P2P Internet currency based on Bitcoin but easier to mine"
HOMEPAGE="https://litecoin.org/"
SRC_URI="https://github.com/${MyPN}-project/${MyPN}/archive/v${MyPV}.tar.gz -> ${MyP}.tar.gz"

RESTRICT="mirror"
LICENSE="MIT ISC GPL-3 LGPL-2.1 public-domain || ( CC-BY-SA-3.0 LGPL-2.1 )"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="+dbus kde +qrcode upnp zmq"

RDEPEND="
	dev-libs/boost:=[threads(+)]
	dev-libs/openssl:0[-bindist]
	dev-libs/protobuf:=
	qrcode? (
		media-gfx/qrencode
	)
	upnp? (
		net-libs/miniupnpc
	)
	sys-libs/db:$(db_ver_to_slot "${DB_VER}")[cxx]
	>=dev-libs/leveldb-1.18-r1
		dev-qt/qtnetwork:5[ssl]
		dev-qt/qtgui:5
		dev-qt/qtwidgets:5
		dbus? (
			dev-qt/qtdbus:5
		)
	zmq? ( net-libs/zeromq )
"
DEPEND="${RDEPEND}
	dev-qt/linguist-tools
	>=app-shells/bash-4.1
	dev-libs/libevent
"

DOCS="doc/README.md"

S="${WORKDIR}/${MyP}"

src_prepare() {
	eautoreconf
	eapply_user
	#rm -r src/leveldb

	cd src || die
}

src_configure() {
	local my_econf=
	if use upnp; then
		my_econf="${my_econf} --with-miniupnpc --enable-upnp-default"
	else
		my_econf="${my_econf} --without-miniupnpc --disable-upnp-default"
	fi
	econf \
		--enable-wallet \
		--disable-ccache \
		--disable-static \
		--disable-tests \
		--with-system-leveldb \
		--with-system-libsecp256k1  \
		--without-libs \
		--without-utils \
		--without-daemon  \
		--with-gui=qt5 \
		$(use_with dbus qtdbus)  \
		$(use_with qrcode qrencode)  \
		$(use_enable zmq zmq) \
		${my_econf}
}

src_install() {
	default
	insinto /usr/share/pixmaps
	newins "share/pixmaps/bitcoin.ico" "${PN}.ico"

	make_desktop_entry "${PN} %u" "Litecoin-Qt" "/usr/share/pixmaps/${PN}.ico" "Qt;Network;P2P;Office;Finance;" "MimeType=x-scheme-handler/litecoin;\nTerminal=false"

	dodoc doc/assets-attribution.md doc/bips.md doc/tor.md
	use zmq && dodoc doc/zmq.md

}

update_caches() {
	gnome2_icon_cache_update
	fdo-mime_desktop_database_update
}

pkg_postinst() {
	update_caches
}

pkg_postrm() {
	update_caches
}
